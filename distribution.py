from scipy import stats
import random


class Distribution:
	def __init__(self, main, mu, sigma):
		self.main = main
		self.mu, self.sigma = mu, sigma
		self.dist = stats.lognorm

	def pdf(self, x):
		return self.dist.pdf(x, self.sigma, scale=self.mu)

	def ppf(self, q):
		return self.dist.ppf(q, self.sigma, scale=self.mu)

	def inverse_function(self, count, minimum, maximum) -> list:
		values = []
		for i in range(count):
			q = random.random()
			x = self.ppf(q)
			if minimum < x < maximum:
				values.append(x)
		return values

	def piecewise_approximation(self, n, count) -> list:
		minimum, step, max_area = .0001, .001, 1 / n
		x, result = [minimum], []
		a, b = minimum, 20
		while a < b:
			ai, sum = a + step, 0
			while sum < max_area:
				prob_1, prob_2 = self.pdf(a), self.pdf(ai)
				area = ((prob_2 + prob_1) / 2) * (ai - a)
				if area < max_area:
					sum = area
					ai += step
				else:
					x.append(round(ai, 2))
					break
			a = ai

		i = 0
		while i < count:
			eps = random.random()
			number = int((n - 1) * eps + 1)
			# if number < 31:
			ak, ak1 = x[number], x[number + 1]
			eps2 = random.random()
			res = ak + (ak1 - ak) * eps2
			result.append(res)
			i += 1
		return result

	def neiman(self, count):
		result, i = [], 0
		a, b = .001, 19
		while i < count:
			x1, x2 = random.random(), random.random()
			x1_, x2_ = a + (b - a) * x1, x2 * self.main.M
			if self.pdf(x1_) > x2_:
				result.append(x1_)
				i += 1
		return result
