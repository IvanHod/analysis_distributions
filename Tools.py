import math
import logging as log


class Tools:
	""""""
	@staticmethod
	def core_smoothing(xx, dist, b=1) -> list:
		N = len(dist)
		yy = []
		for x in xx:
			y = sum(Tools.core_function((val - x) / b) for val in dist) / (N * b)
			yy.append(y)
		return yy

	reverse_sqrt_2pi = 1 / math.sqrt(2 * math.pi)

	@staticmethod
	def core_function(u) -> float:
		return Tools.reverse_sqrt_2pi * math.exp(-0.5 * (u ** 2))

	# u, o = .9983, .9959  # подобранные
	# u, o = .89356, .54758  # метод моментов автоматическое решение
	u, o = .704653, .82746  # метод моментов, моё решение
	mu, sigma = .704653, .82746

	@staticmethod
	def lognormal(xx) -> list:
		o_sqrt_2pi = Tools.o * math.sqrt(2 * math.pi)
		doubled_o = 2 * (Tools.o ** 2)

		yy = []
		for x in xx:
			if x == 0:
				x = .00001
			y = math.exp(-math.pow(math.log(x) - Tools.u, 2) / doubled_o) / (x * o_sqrt_2pi)
			yy.append(y)
		return yy

	@staticmethod
	def calculate_params_max_likelihood(array):
		array = list(filter(lambda v: v > .0, map(lambda v: float(v['Trip_distance']), array)))
		N = len(array)
		middle = sum(math.log(v) for v in array) / N
		count = {}
		for v in array:
			count[v] = count[v] + 1 if v in count else 1
		middle_o2 = 0
		for v in array:
			middle_o2 += (math.log(v) - middle) ** 2
		middle_o2 /= N
		o = math.sqrt(middle_o2)
		Tools.u = middle
		Tools.o = o
		log.info('Max likelihood method: mu={}, sigma: {}'.format(Tools.u, o))

	@staticmethod
	def calculate_params_quantiles(quantiles):
		Tools.u = math.fabs(math.log(2.849083))  # math.log(quantiles['middle']))
		Tools.a = math.fabs(math.log(quantiles['lower'] / quantiles['upper']) / 1.35)
		log.info('Quantiles method: mu={}, sigma: {}'.format(Tools.u, Tools.o))
