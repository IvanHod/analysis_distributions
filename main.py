# coding=utf-8
import sys
import logging as log
import matplotlib.pyplot as plt
from scipy import stats
import numpy as np
import math
from Tools import Tools
from distribution import Distribution
import random
import statsmodels.api as sm

from dataHelper import DataHelper


def test_func(main):
	distances = main.get_distances()
	xx = np.linspace(min(distances), max(distances), int(max(distances) - min(distances)) * 5)

	plt_, ax = plt.subplots(figsize=(8, 6))
	ax.hist(distances, normed=True, bins=20, alpha=0.3)
	plt.xticks(range(22))

	kde = stats.gaussian_kde(distances)
	kde.set_bandwidth(bw_method=kde.factor)
	ax.plot(xx, kde(xx), color='r', label='Method gaussian kde')

	yy_05 = Tools.core_smoothing(xx, distances, .5)  # b = 0.5
	yy = Tools.core_smoothing(xx, distances, .8)  # b = 1
	yy_15 = Tools.core_smoothing(xx, distances, 1.5)  # b = 1.5
	ax.plot(xx, yy_05, color='y', alpha=.8, label='Band width is 0.5')
	ax.plot(xx, yy, color='g', alpha=.8, label='Band width is 1.0')
	ax.plot(xx, yy_15, color='b', alpha=.8, label='Band width is 1.5')

	plt.xlabel('Length of way, km')
	plt.ylabel('Probability')
	plt.legend()
	plt_.show()


class Main:
	data = None
	quantiles = {}

	def __init__(self, csv_file='2015_Green_Taxi_Trip_Data.csv', short_csv_file='taxi_data'):
		self.csv_file_name = csv_file
		self.short_csv_file = short_csv_file

		self.data_helper = DataHelper(csv_file, short_csv_file)
		self.M = None
		self.D = None

	def read_data(self):
		if self.data_helper.is_exist_short_csv():
			self.data = self.data_helper.read_short_csv()
		else:
			self.data = self.data_helper.read_csv()
			self.data_helper.to_short(self.data)

	def calculate_stats(self):
		data = list(sorted(filter(lambda v: v > 0, map(lambda v: float(v['Trip_distance']), self.data))))
		length = len(self.data)
		self.M = expected_value = sum(data) / length
		self.D = dispersion = math.sqrt((sum(math.pow(val - expected_value, 2) for val in data)) / (length - 1))

		log.info("Expected value: {}. Dispersion: {}.".format(expected_value, dispersion))
		self.quantiles['lower'] = np.percentile(data, 25)
		self.quantiles['middle'] = np.percentile(data, 50)
		self.quantiles['upper'] = np.percentile(data, 75)
		log.info("Lower quantile: {}. Middle quantile: {}. Upper quantile: {}".format(self.quantiles['lower'],
		                                                                              self.quantiles['middle'],
		                                                                              self.quantiles['upper']))

	def get_distances(self):
		return list(map(lambda x: round(float(x['Trip_distance']), 1), self.data))

	def calculate_params(self):
		distances = self.get_distances()
		xx = np.linspace(min(distances), max(distances), int(max(distances) - min(distances)) * 5)
		plt_, ax = plt.subplots(figsize=(8, 6))

		yy_ = Tools.core_smoothing(xx, distances, 0.6)  # b = 1
		ax.plot(xx, yy_, color='y', alpha=.8, label='Band width is 0.6')

		yy_ = Tools.core_smoothing(xx, distances, .8)  # b = 1
		ax.plot(xx, yy_, color='g', alpha=.8, label='Band width is 0.8')

		yy_ = Tools.core_smoothing(xx, distances, 1.2)  # b = 1
		ax.plot(xx, yy_, color='b', alpha=.8, label='Band width is 1.2')

		Tools.calculate_params_max_likelihood(self.data)
		Tools.calculate_params_quantiles(self.quantiles)
		yy = Tools.lognormal(xx)
		ax.plot(xx, yy, color='r', alpha=.8, label='Method of quantiles')
		# ax.plot(xx, yy, color='r', alpha=.8, label='Method of moments')
		# ax.plot(xx, yy, color='r', alpha=.8, label='Maximum likelihood method')
		plt.legend()
		plt.show()

	def show_qq_plot(self):
		plt_, ax = plt.subplots(figsize=(8, 6))
		data = list(filter(lambda v: v > 0, map(lambda v: float(v['Trip_distance']), main.data)))
		sample = np.array(random.sample(data, 100))
		sm.qqplot(sample, line='45', dist=stats.lognorm, distargs=(Tools.o,), scale=math.exp(Tools.u), ax=ax)
		plt.show()

	@staticmethod
	def normalize(series: list):
		map_series = {}
		for num in series:
			map_series[num] = map_series[num] + 1 if num in map_series else 1

		x, y = [], []
		N = len(series)
		for num in map_series.items():
			x.append(num[0])
			y.append(num[1] / N)

		return x, y


if __name__ == '__main__':
	formater = u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s'
	log.basicConfig(format=formater, level=log.DEBUG)

	main = Main()
	main.read_data()
	main.calculate_stats()

	# test_func(main)
	# main.calculate_params()
	# main.show_qq_plot()

	# stats.lognorm.rvs()  # random variable simulation
	Tools.calculate_params_max_likelihood(main.data)
	Tools.calculate_params_quantiles(main.quantiles)

	dist = Distribution(main, math.exp(Tools.u), Tools.o)
	# values, title = dist.inverse_function(500, 0.001, 20), "Method on inverse function"
	values, title = dist.piecewise_approximation(32, 500), "Method of Piece-wise discretisation"
	# values, title = dist.neiman(500), "Method of Neiman"

	ax = plt.subplot()
	ax.hist(values, bins=26, normed=True, alpha=.6)
	ax.set_title(title)
	ax.set_xlabel("Length of way, km")
	ax.set_ylabel("Probability")
	plt.grid()
	plt.show()

	sys.exit()
