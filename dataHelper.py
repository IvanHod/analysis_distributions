import os.path as path
import logging as log
import re


class DataHelper:

	COUNT = 500

	titles = ['rate_code', 'Passenger_count', 'Trip_distance', 'Fare_amount', 'Total_amount', 'Payment_type', 'Trip_type']

	def __init__(self, csv_file, csv_short_file_name):
		self.csv_file_name = csv_file
		self.csv_short_file_name = csv_short_file_name

	def is_exist_short_csv(self):
		return path.exists(self.csv_short_file_name)

	def read_short_csv(self):
		log.info('Reading short csv file: '.format(self.csv_short_file_name))
		f = open(self.csv_file_name, 'r')
		data = []
		titles = f.readline().replace('\n', '').split(',')
		for line in f:
			data.append(self.get_object(titles, line.replace('\n', '').split(',')))

		f.close()
		return data

	def get_object(self, titles: list, line_split: list):
		obj = {}
		for title in self.titles:
			obj[title] = line_split[titles.index(title)]
		return obj

	def read_csv(self):
		log.info('Reading csv file: '.format(self.csv_file_name))
		f = open(self.csv_file_name)
		data = []
		titles = f.readline().replace('\n', '').split(',')
		index = 0
		for line in f:
			data.append(self.get_object(titles, line.replace('\n', '').split(',')))
			if index > self.COUNT:
				break
			index += 1

		f.close()
		return data

	def to_short(self, data):
		log.info('Writing short csv file...')
		f = open(self.get_short_name(), 'w')
		f.write(','.join(self.titles) + '\n')
		for d in data:
			line = ''
			for title in self.titles:
				line += d[title] + ','
			f.write(re.sub(',$', '\n', line))
		f.close()

	def get_short_name(self):
		return '{}_{}.csv'.format(self.csv_short_file_name, self.COUNT)
